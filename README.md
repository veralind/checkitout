# Check it Out 

Spotify plugin for checking out artists. 
Finds artist by name and adds their 5 most recent tracks to a private playlist.

## Check out the tech
* Java
* Spring Boot
* CI/CD via gitlab: 
   * automated build
   * Docker containerization
   * deployment in Kubernetes

## Check out demo video
![](checkitout.mp4)