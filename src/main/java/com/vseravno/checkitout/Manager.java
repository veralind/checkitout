package com.vseravno.checkitout;

import com.google.common.util.concurrent.RateLimiter;
import com.google.gson.JsonArray;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.exceptions.detailed.NotFoundException;
import com.wrapper.spotify.model_objects.specification.*;
import org.apache.hc.core5.http.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public class Manager {
  private static final Logger logger = LoggerFactory.getLogger(Manager.class);
  private final String dynamicPlaylistSid = "5VIJYlZR08VYesfdX46OEQ";
  private final String mildPlaylistSid = "3W2spb2KROLl9VOxEk9Of0";
  private final String uaPlaylistSid = "6ml4OXo7M11gSmGpBEFxpa";
  private final int FETCH_LIMIT = 50;
  private SpotifyApi api;
  private String artistName;
  private String artistSid;
  private String playlist;
  static RateLimiter rateLimiter = RateLimiter.create(5);

  public Manager(SpotifyApi api, String artistSid, String artistName, String playlist) {
    this.api = api;
    this.artistSid = artistSid;
    this.artistName = artistName;
    if (playlist.equals("1")) {
      this.playlist = dynamicPlaylistSid;
    } else if (playlist.equals("2")) {
      this.playlist = mildPlaylistSid;
    } else if (playlist.equals("3")) {
      this.playlist = uaPlaylistSid;
    }
  }

  public Manager(SpotifyApi api, String artistName) {
    this.api = api;
    this.artistName = artistName;
  }

  public Manager(SpotifyApi api) {
    this.api = api;
  }

  public Artist[] getArtist() {
    try {
      Paging<Artist> artistPaging = api.searchArtists(artistName).build().execute();
      Integer total = artistPaging.getTotal();
      if (total < 1) {
        return null;
      } else {
        return artistPaging.getItems();
      }
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException(e);
    }
  }

  public void addTracks() {
    Set<TrackSimplified> tracks = prepareTracks();
    addTracksToPlaylist(tracks);
  }

  private void addTracksToPlaylist(Set<TrackSimplified> tracks) {
    Set<String> uris = new HashSet<>();
    JsonArray jsonUris = new JsonArray();
    for (TrackSimplified track : tracks) {
      uris.add(track.getUri());
    }
    for (String s : uris) {
      jsonUris.add(s);
    }
    try {
      api.addItemsToPlaylist(playlist, jsonUris).build().execute();
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException(e);
    }
  }

  private Set<TrackSimplified> prepareTracks() {
    Set<AlbumSimplified> sevenRecentAlbums = getSevenRecentAlbums(artistSid);
    rateLimiter.acquire();
    return getFiveTracks(sevenRecentAlbums);
  }

  private Set<AlbumSimplified> getSevenRecentAlbums(String artistSid) {
    // getting all albums first
    try {
      Paging<AlbumSimplified> albumsPaging =
          api.getArtistsAlbums(artistSid)
              .album_type("album,single")
              .limit(FETCH_LIMIT)
              .build()
              .execute();
      Integer total = albumsPaging.getTotal();
      ArrayList<AlbumSimplified> albums = new ArrayList<>(Arrays.asList(albumsPaging.getItems()));

      for (int offset = FETCH_LIMIT; offset < total; offset += FETCH_LIMIT) {
        Paging<AlbumSimplified> moreAlbums =
            api.getArtistsAlbums(artistSid).offset(offset).build().execute();
        albums.addAll(Arrays.asList(moreAlbums.getItems()));
      }
      return filterAlbums(albums);

    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException(e);
    }
  }

  private Set<AlbumSimplified> filterAlbums(ArrayList<AlbumSimplified> albums) {
    Set<AlbumSimplified> albumsWithFullDate =
        albums.stream()
            .filter(album -> album.getReleaseDate().length() == 10)
            .collect(Collectors.toSet());
    Set<AlbumSimplified> filteredAlbums = filterDuplicateAlbumsByName(albumsWithFullDate);
    return filteredAlbums.stream()
        .filter(this::lastTwoYearsAlbum)
        .sorted(Comparator.comparing(AlbumSimplified::getReleaseDate).reversed())
        .limit(7)
        .collect(Collectors.toSet());
  }

  private Set<AlbumSimplified> filterDuplicateAlbumsByName(Set<AlbumSimplified> albums) {
    Set<String> existingNames = new HashSet<>();
    albums.removeIf(a -> !existingNames.add(a.getName()));
    return albums;
  }

  private boolean lastTwoYearsAlbum(AlbumSimplified album) {
    LocalDate albumDate = LocalDate.parse(album.getReleaseDate());
    long releasePeriod = ChronoUnit.MONTHS.between(albumDate, LocalDate.now());
    // return albums released 2 years ago max
    return releasePeriod <= 24;
  }

  private Set<TrackSimplified> getFiveTracks(Set<AlbumSimplified> albums) {
    Set<TrackSimplified> tracks = new HashSet<>();
    Set<String> filteredTracksByName = new HashSet<>();

    for (AlbumSimplified album : albums) {
      // get tracks from album
      List<TrackSimplified> albumTracks = getAlbumTracks(album);
      for (TrackSimplified albumTrack : albumTracks) {
        boolean artistpresent = false;
        ArtistSimplified[] artists = albumTrack.getArtists();
        for (ArtistSimplified artistSimplified : artists) {
          if (artistSimplified.getName().equals(artistName)) {
            artistpresent = true;
            break;
          }
        }
        // filter tracks by name (using set)
        if (filteredTracksByName.add(albumTrack.getName()) && artistpresent) {
          tracks.add(albumTrack);
          break;
        }
      }
      if (tracks.size() >= 5) {
        break;
      }
    }
    return tracks;
  }

  private List<TrackSimplified> getAlbumTracks(AlbumSimplified album) {
    try {
      Paging<TrackSimplified> pagingTracks =
          api.getAlbumsTracks(album.getId()).limit(50).build().execute();
      return Arrays.asList(pagingTracks.getItems());
    } catch (NotFoundException e) {
      logger.warn("Failed to get tracks from an album {}", album.getName());
      return List.of();
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      throw new RuntimeException(e);
    }
  }
}
