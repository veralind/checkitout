package com.vseravno.checkitout;

import com.google.common.io.Resources;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.model_objects.specification.Artist;
import com.wrapper.spotify.model_objects.specification.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@RestController
public class Controller {
  private static final Logger logger = LoggerFactory.getLogger(Controller.class);
  private final ApiBuilder apiBuilder;

  @Autowired
  public Controller(ApiBuilder apiBuilder) {
    this.apiBuilder = apiBuilder;
  }

  @GetMapping("/")
  String choosePlaylist() throws IOException {
    return Resources.toString(Resources.getResource("index.html"), StandardCharsets.UTF_8);
  }

  @GetMapping("/search/{number}")
  String getArtistInput(@PathVariable("number") String number) throws IOException {
    return Resources.toString(Resources.getResource("searchform.html"), StandardCharsets.UTF_8)
        .replace("number", number);
  }

  @RequestMapping(value = "/artists/{number}", method = RequestMethod.POST)
  String showArtists(
      @PathVariable("number") String number, @RequestParam("artist") String artistName) {
    SpotifyApi api = apiBuilder.getApi();
    Manager manager = new Manager(api, artistName);
    Artist[] artists = manager.getArtist();
    if (artists == null) {
      return HtmlBuilder.errorMsg();
    } else {
      String htmlBody = "";
      for (Artist artist : artists) {
        String artistForm = HtmlBuilder.buildArtistForm(artist, number);
        htmlBody = htmlBody.concat(artistForm);
      }
      return HtmlBuilder.buildPage(htmlBody);
    }
  }

  @RequestMapping(value = "/addTracks/{number}", method = RequestMethod.POST)
  String update(
      @PathVariable("number") String number,
      @RequestParam("chosenArtistSid") String artistSid,
      @RequestParam("chosenArtistName") String artistName) {
    try {
      Manager manager = new Manager(apiBuilder.getApi(), artistSid, artistName, number);
      manager.addTracks();
      return HtmlBuilder.successMsg();
    } catch (Exception e) {
      logger.error("Could not add tracks from artist {}", artistName, e);
      return HtmlBuilder.errorMsg();
    }
  }
}
