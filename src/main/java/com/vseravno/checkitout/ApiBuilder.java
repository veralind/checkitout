package com.vseravno.checkitout;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.AuthorizationCodeCredentials;
import org.apache.hc.core5.http.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ApiBuilder {

  private static final String CLIENT_ID = "ab7439db0812454ba3b8d2a6170b1459";
  private final YAMLConfig yamlConfig;

  @Autowired
  public ApiBuilder(YAMLConfig yamlConfig) {
    this.yamlConfig = yamlConfig;
  }

  public SpotifyApi getApi() {
    try {
      SpotifyApi api =
          new SpotifyApi.Builder()
              .setClientId(CLIENT_ID)
              .setClientSecret(yamlConfig.getClientSecret())
              .setRefreshToken(yamlConfig.getRefreshToken())
              .build();
      AuthorizationCodeCredentials tokens = api.authorizationCodeRefresh().build().execute();
      api.setAccessToken(tokens.getAccessToken());
      return api;
    } catch (IOException | SpotifyWebApiException | ParseException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }
}
